<?php get_header(); ?>
<div class="alert alert-danger alert-message" role="alert">
  <h4 class="alert-heading">important!</h4>
  <p >Please  <a href='<?php echo WP_REACT_URL ?> ' >click here</a> to use our web site.</p>
  <hr>
</div>
<?php get_footer(); ?>