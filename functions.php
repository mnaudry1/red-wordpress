<?php

function red_scripts() {
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css', array(), '5.1.3' );
	wp_enqueue_style( 'blog', get_template_directory_uri() . '/css/blog.css' );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.js', array( 'jquery' ), '5.1.3', true );
}
add_action( 'wp_enqueue_scripts', 'red_scripts' );

function red_theme_support() {
    $header_info = array(
    'width'         => 980,
    'height'        => 600,
    'default-image' => get_template_directory_uri() . '/images/cuisine.jpg',
    );
    add_theme_support( 'custom-header', $header_info );
    add_theme_support( 'post-thumbnails', array( 'post' ) ); 

    set_post_thumbnail_size( 980, 600,true);
	add_image_size( 'red-thumbnail', 200, 200 );
    add_image_size( 'red-featured-image', 980, 600 );
}

add_action( 'after_setup_theme', 'red_theme_support' );




function get_custom_header_api() {
    // rest_ensure_response() wraps the data we want to return into a WP_REST_Response, and ensures it will be properly returned.
    //return rest_ensure_response( 'Hello World, this is the WordPress REST API' );

    $custom_header = 'no content';

    if(get_header_image() ){
        $custom_header = array(
            'src' => esc_url( get_header_image()),
            'width' => get_custom_header()->width,
            'height' => get_custom_header()->height,
        );
        return create_response($custom_header);
    } else {

        return create_response_error($custom_header,204);
    }

    return $custom_header;
}

function get_last_post_api(){

    $_posts = get_posts();
    $last_posts = [];
    $reponse = null ;
    foreach($_posts as $_post){
        $post = array();
        $post['id'] = $_post->ID;
        $post['author'] = get_the_author_meta('display_name',$_post->post_author);
        $post['thumbnail'] =get_thumbail_by_size($_post->ID,'red-thumbnail');
        $post['title'] = $_post->post_title;
        $post['excerpt'] = $_post->post_excerpt;
        $post['published_at'] = $_post->post_date;
        array_push($last_posts, $post);
    }

    if(!count($last_posts)){
        return create_response_error('no posts',204);
    }

    return create_response($last_posts);
}


function get_post_byid($request){
    $_post = get_post($request['post_id']);
    if(empty($_post)){
        return create_response_error('post not find',404);
    } else {
        $post['id'] = $_post->ID;
        $post['author'] = get_the_author_meta('display_name',$_post->post_author);
        $post['featured_image'] =get_thumbail_by_size($_post->ID,'red-featured-image');
        $post['title'] = $_post->post_title;
        $post['published_at'] = $_post->post_date;
        $post['content'] = $_post->post_content;
        return create_response($post);
    }
}

function create_response_error($data , $code = 404){
    $response = new WP_REST_Response( array('error' => $data) );
    $response->set_status($code);

    return $response;

}

function create_response($data){
    $response = new WP_REST_Response( $data);
    $response->set_status( 200 );

    return $response;
}


function get_thumbail_by_size($post_id,$size){
   $thumbnail = array();
    switch($size){
        case 'red-thumbnail' :
            $thumbnail['src'] = get_the_post_thumbnail_url($post_id,'red-thumbnail');
            $thumbnail['height'] = 200;
            $thumbnail['width'] = 200;
            break;
        case 'red-featured-image':
            $thumbnail['src'] = get_the_post_thumbnail_url($post_id,'red-featured-image');
            $thumbnail['height'] = 980;
            $thumbnail['width'] = 600;
            break;
        default :
            $thumbnail['src'] = get_the_post_thumbnail_url($post_id,'red-thumbnail');
            $thumbnail['height'] = 980;
            $thumbnail['width'] = 600;
    }
    

    return $thumbnail;
}



 
/**
 * This function is where we register our routes for our example endpoint.
 */
function red_custom_header() {
    // register_rest_route() handles more arguments but we are going to stick to the basics for now.
    register_rest_route( 'red/v1', '/custom-header', array(
        // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
        'methods'  => 'GET',
        // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
        'callback' => 'get_custom_header_api',
    ) );
}

function red_last_posts() {

    register_rest_route( 'red/v1', '/last-posts', array(
        // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
        'methods'  => 'GET',
        // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
        'callback' => 'get_last_post_api',
    ) );

}


function red_post_content($request) {

    register_rest_route( 'red/v1', '/post/(?P<post_id>\d+)', array(
        // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
        'methods'  => 'GET',
        // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
        'callback' => 'get_post_byid',
    ) );

}
 
add_action( 'rest_api_init', 'red_custom_header' );
add_action( 'rest_api_init', 'red_last_posts' );
add_action( 'rest_api_init', 'red_post_content' );